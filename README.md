3DMaze
------

3D maze is a generator program, which uses WorldGenerator project to output specific
type of geometry. The purpose is to use pymaze tool to generate a 2D maze image,
and then reconstruct a 3D cylinder using that maze as a displacement on the
surface of the cylinder.

What is the purpose of this black magic..? The idea is to create a 3D printable
object, which can be directly fed into 3D printing software. 3D cylinder
mazes are fun, and if you have 3D printed available this is an unlimited source
of fun. lol.

Quick instructions for runnign:

Clone both WorldGenerator and 3DMaze projects. Setup PYTHONPATH.
```
$ cd to/your/project/folder
$ git clone git@bitbucket.org:swcv/worldgenerator.git
$ git clone git@bitbucket.org:swcv/3dmaze.git
$ export PYTHONPATH=$PYTHONPATH:`pwd`/worldgenerator
$ cd 3dmaze
```
Setup and activate python virtual environment:
```
$ make virtualenv
$ source env/bin/activate
```
Generate new Maze:
```
$ make maze
```
Generate new printable 3d object:
```
$ make 3dobject
```
![IMG-20170106-WA0000.jpg](https://bitbucket.org/repo/7jr8zq/images/1375933763-IMG-20170106-WA0000.jpg)