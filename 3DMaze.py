#!/usr/bin/env python3
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os, math

# From worldgenerator
try:
    import MeshContainer
    import MeshIO
    import MeshGenerator
except ImportError as e:
    print ("Meshtools import failed. WorldGenerator is needed in PYTHONPATH!")
    print ("(%s)" % repr(e))
    sys.exit(0)

from PIL import Image as i
from PIL import ImageFilter

# x is [0..1]
# y is [0..PI]
def cylinderCallback(x, y, data):
    tx = data['w'] * x
    ty = data['h'] * y / (2.0*math.pi)
    p = data['image'].load()[tx, ty]
    p = sum(p) / len(p)
    p = p / (255.0 / 0.08) + 0.42
    return p

###############################################################################
#

class Generator():

    def __init__(self, maze, width, height, depth):
        self.maze = i.open(maze).filter(ImageFilter.GaussianBlur(radius=7))
        self.maze.save('temp.png')
        self.mazeW = self.maze.width
        self.mazeH = self.maze.height

        self.mesh = MeshContainer.MeshContainer()
        meshgen = MeshGenerator.MeshGenerator(self.mesh, sharedgeometry=False)
        meshgen.createCylinder(slices=300, LOD=60, r2=0.375, callback=cylinderCallback, callbackdata={'image':self.maze, 'w':self.mazeW, 'h':self.mazeH})
        self.mesh.scale(width, height, depth)
        self.mesh.rotate(90, 1, 0, 0)

    def toFile(self, output):
        meshio = MeshIO.MeshIO(self.mesh)
        print ("Trying output write to %s" % output)
        meshio.toFile(output, overwrite=True)


if __name__ == "__main__":
    import getopt

    def usage():
        print ("3DMaze [options]")
        print (" -w|--width      Width of target cylinder in mm")
        print (" -h|--height     Height of target cylinder in mm")
        print (" -d|--depth      Depth of target cylinder in mm")
        print (" -m|--maze       Input maze imagefile")
        print (" -o|--output     Output OBJ file, ready for printing")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "w:h:d:m:o:",
            ["width=", "height=", "depth=", "maze=", "output="])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    width   = 35.0
    height  = 95.0
    depth   = 35.0
    maze    = 'maze.png'
    output  = '3dmaze.obj'
    for o, a in opts:
        if o in ("-w", "--width"):      width = float(a)
        if o in ("-h", "--height"):     height = float(a)
        if o in ("-d", "--depth"):      depth = float(a)
        if o in ("-m", "--maze"):       maze = str(a)
        if o in ("-o", "--output"):     output = str(a)

    g = Generator(maze, width, height, depth)
    g.toFile(output)

