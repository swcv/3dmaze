default: info

info:
	@echo "Available targets:"
	@echo "virtualenv:   Create new virtual environment"
	@echo "maze:         Generate new maze.png"
	@echo "3dobject:     Generate cylinder with mapped maze image"

virtualenv:
	/bin/bash -c 'rm -rf env'
	/bin/bash -c 'virtualenv -p python2 env'
	/bin/bash -c 'source env/bin/activate && pip install -r requirements.txt && deactivate'
	@echo "Done!"

maze:
	/bin/bash -c 'source env/bin/activate && amaze --maze-size 16 16 --image-output maze.png --image-wall-width 20 --image-path-width 20 --image-path-smooth --image-path-color=#00000000 && deactivate'
	@echo "Done!"

3dobject:
	/bin/bash -c 'python 3DMaze.py -w 36 -h 95 -d 35 -m maze.png -o 3dmaze.obj'

